package org.apache.bookkeeper.mledger.impl;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;
import com.google.common.base.Charsets;
import io.netty.buffer.ByteBuf;
import java.nio.charset.Charset;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import lombok.SneakyThrows;
import org.apache.bookkeeper.mledger.AsyncCallbacks;
import org.apache.bookkeeper.mledger.ManagedCursor;
import org.apache.bookkeeper.mledger.ManagedLedger;
import org.apache.bookkeeper.mledger.ManagedLedgerConfig;
import org.apache.bookkeeper.mledger.ManagedLedgerException;
import org.apache.bookkeeper.mledger.Position;
import org.apache.bookkeeper.test.MockedBookKeeperRolloverRateTestCase;
import org.testng.annotations.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.DataProvider;

public class LedgerRolloverLimiterTest extends MockedBookKeeperRolloverRateTestCase {

    private static final Logger log = LoggerFactory.getLogger(LedgerRolloverLimiterTest.class);

    private static final Charset Encoding = Charsets.UTF_8;

    @DataProvider(name = "checkOwnershipFlag")
    public Object[][] checkOwnershipFlagProvider() {
        return new Object[][] { { Boolean.TRUE }, { Boolean.FALSE } };
    }

    @Test
    public void simpleTest() throws ManagedLedgerException, InterruptedException {
        ManagedLedgerConfig config = new ManagedLedgerConfig();
        config.setMaxEntriesPerLedger(2);

        ManagedLedgerImpl ledger = (ManagedLedgerImpl) factory.open("rollover-test-ledger-1", config);
        assertEquals(ledger.getNumberOfEntries(), 0);
        for (int i = 0; i < 100; i++) {
            ledger.addEntry("data".getBytes(Encoding));
            // The limiter just doesn't work
            Thread.sleep(200);
        }
        assertEquals(ledger.getEntriesAddedCounter(), 100);
        ledger.close();
        Thread.sleep(2000);
        factory.shutdown();
    }

    @Test
    public void simpleTestV2() throws Exception{
        ManagedLedgerConfig config = new ManagedLedgerConfig()
                .setMaxEntriesPerLedger(2);
        config.setMaximumRolloverTime(600, TimeUnit.SECONDS);

        for (int i = 0; i < 10; i++) {
            MyThread thread = new MyThread(config, i, 0);
            thread.start();
        }
        Thread.sleep(24000);
    }

    class MyThread extends Thread {

        private final ManagedLedger managedLedger;
        private final String threadName;
        private final ManagedCursor cursor;
        private final Integer delay;

        public MyThread (ManagedLedgerConfig config, Integer i, Integer delay)
                throws ManagedLedgerException, InterruptedException {
            this.threadName = "thread-ledger" + i;
            managedLedger = factory.open(threadName, config);
            cursor = managedLedger.openCursor("cursor" + i);
            this.delay = delay;
        }

        @SneakyThrows
        @Override
        public void run() {
            for (int i = 0; i < 2; i++) {
                managedLedger.addEntry(threadName.getBytes());
                sleep(delay);
            }
            assertTrue((((ManagedLedgerImpl) managedLedger).ledgers.size()) == 1);
            assertEquals((((ManagedLedgerImpl) managedLedger).getEntriesAddedCounter()), 2);
        }
    }

    @Test
    public void testAsyncAddEntryAndSyncClose() throws Exception {
        ManagedLedgerConfig config = new ManagedLedgerConfig().setMaxEntriesPerLedger(10);
        ManagedLedger ledger = factory.open("my_test_ledger", config);
        ledger.openCursor("c1");

        assertEquals(ledger.getNumberOfEntries(), 0);

        final CountDownLatch counter = new CountDownLatch(2000);

        for (int i = 0; i < 2000; i++) {
            String content = "entry-" + i;
            ledger.asyncAddEntry(content.getBytes(Encoding), new AsyncCallbacks.AddEntryCallback() {
                @Override
                public void addComplete(Position position, ByteBuf entryData, Object ctx) {
                    counter.countDown();
                }

                @Override
                public void addFailed(ManagedLedgerException exception, Object ctx) {
                    fail(exception.getMessage());
                }

            }, null);
        }

        counter.await();

        assertEquals(ledger.getNumberOfEntries(), 2000);
        assertEquals(((ManagedLedgerImpl) ledger).getCurrentLedgerEntries(), 10);
    }
}
