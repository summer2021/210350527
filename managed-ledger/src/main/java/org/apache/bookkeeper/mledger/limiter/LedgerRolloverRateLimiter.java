package org.apache.bookkeeper.mledger.limiter;

import io.netty.util.concurrent.DefaultThreadFactory;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.apache.bookkeeper.mledger.ManagedLedger;
import org.apache.bookkeeper.mledger.ManagedLedgerConfig;
import org.apache.bookkeeper.mledger.ManagedLedgerFactory;
import org.apache.bookkeeper.mledger.ManagedLedgerFactoryConfig;
import org.apache.bookkeeper.mledger.impl.ManagedLedgerFactoryImpl;
import org.apache.pulsar.common.policies.data.RolloverRate;
import org.apache.pulsar.common.util.RateLimiter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author SJQ
 */
public class LedgerRolloverRateLimiter {
    private RolloverRate rolloverRate;
    private RateLimiter ledgerRolloverRateLimiter;
    /**
     * Start a scheduled task.
     */
    private final ScheduledExecutorService executorService;
    private ScheduledFuture<?> resetTask;
    private ManagedLedgerFactoryConfig config;
    private LedgerRolloverScheduler ledgerRolloverSchedule;
    private Set<ManagedLedger> ledgersHasPerm;

    /**
     * double-checked locking to get singleton.
     */
    private static volatile LedgerRolloverRateLimiter singleton;

    public static LedgerRolloverRateLimiter getLedgerRolloverRateLimiter(ManagedLedgerFactoryConfig config) {
        if (singleton == null) {
            synchronized (LedgerRolloverRateLimiter.class) {
                if (singleton == null) {
                    singleton = new LedgerRolloverRateLimiter(config);
                }
            }
        }
        return singleton;
    }

    public static LedgerRolloverRateLimiter getLedgerRolloverRateLimiter() {
        return singleton;
    }

    public LedgerRolloverRateLimiter(ManagedLedgerFactoryImpl factory) {
        this.config = factory.getConfig();
        this.executorService = new ScheduledThreadPoolExecutor(1,
                new DefaultThreadFactory("rollover-limiter"));
        // get rollover rate from config
        this.rolloverRate = new RolloverRate(config.getRolloverThrottlingRate(),
                config.getRolloverRatePeriodInSecond());
        if (isRolloverRateEnabled(this.rolloverRate)) {
            this.ledgerRolloverSchedule = new LedgerRolloverScheduler(this.rolloverRate);
            resetTask = createTask();
            log.info("configured rollover rate at ledger {}", this.rolloverRate);
        }
    }

    /**
     * use managedLedgerConfig to init.
     * @param config managedLedgerConfig
     */
    public LedgerRolloverRateLimiter(ManagedLedgerFactoryConfig config) {
        this.config = config;
        this.executorService = new ScheduledThreadPoolExecutor(5,
                new DefaultThreadFactory("rollover-limiter"));
        this.rolloverRate = new RolloverRate(config.getRolloverThrottlingRate(),
                config.getRolloverRatePeriodInSecond());
        if (isRolloverRateEnabled(this.rolloverRate)) {
            this.ledgerRolloverSchedule = new LedgerRolloverScheduler(this.rolloverRate);
            resetTask = createTask();
            log.info("configured ledger rollover rate {}", this.rolloverRate);
            ledgersHasPerm = new HashSet<>();
        }
    }

    /**
     * return available rollovers if rollover-throttling is enabled else it returns -1.
     *
     * @return long
     */
    public long getAvailableRolloverRateLimit() {
        return ledgerRolloverRateLimiter
                == null ? -1 : ledgerRolloverRateLimiter.getAvailablePermits();
    }

    /**
     * It acquires rollover from rollover-limiter and returns if acquired permits succeed.
     */
    public synchronized boolean tryAcquire(ManagedLedger ledger) {
        log.info("[{}] try acquire...", ledger.getName());
        addRolloverLimiterIfAbsent();
        if (ledgerRolloverRateLimiter == null
                || (this.ledgersHasPerm != null && this.ledgersHasPerm.contains(ledger))){
            return true;
        }
        if (ledgerRolloverRateLimiter.tryAcquire()) {
            this.ledgersHasPerm.add(ledger);
            return true;
        } else {
            this.ledgerRolloverSchedule.add(ledger);
            return false;
        }
    }

    /**
     * checks if rollover-rate limit is configured and if it's configured then check if
     * rollover are available or not.
     *
     * @return {@code true} if available {@code false} else
     */
    public boolean rolloverAvailable(ManagedLedger ledger) {
        log.info("[{}] try to get permission", ledger.getName());
        if (ledgerRolloverSchedule != null && ledgerRolloverSchedule.contains(ledger)) {
            // if the ledger is already in the queue, we consider
            // the ledger is pending rollover. So return false.
            return false;
        }
        if (ledgerRolloverRateLimiter == null
                || ledgerRolloverRateLimiter.getAvailablePermits() > 0) {
            return true;
        } else {
            this.ledgerRolloverSchedule.add(ledger);
            return false;
        }
    }

    /**
     * Update rollover-throttling-rate. applies default broker rollover-throttling-rate.
     */
    private synchronized void addRolloverLimiterIfAbsent() {
        if (ledgerRolloverRateLimiter != null || !isRolloverRateEnabled(rolloverRate)) {
            return;
        }
        updateRolloverRate(this.rolloverRate);
    }

    /**
     * remove rollover limiter.
     */
    private synchronized void removeRolloverLimiter() {
        if (this.ledgerRolloverRateLimiter != null) {
            this.ledgerRolloverRateLimiter.close();
        }
    }

    /**
     * update rollover rate.
     *
     * @param rolloverRate rollover rate
     */
    public synchronized void updateRolloverRate(RolloverRate rolloverRate) {
        long rolloverThrottlingRate = rolloverRate.rolloverThrottlingRate;
        long ratePeriod = rolloverRate.ratePeriodInSecond;

        if (rolloverThrottlingRate > 0) {
            if (this.ledgerRolloverRateLimiter == null) {
                ScheduledExecutorService executorService = new ScheduledThreadPoolExecutor(1,
                        new DefaultThreadFactory("rollover-limiter"));
                this.ledgerRolloverRateLimiter = new RateLimiter(executorService,
                        rolloverThrottlingRate, ratePeriod, TimeUnit.SECONDS, null);
            } else {
                this.ledgerRolloverRateLimiter.setRate(rolloverThrottlingRate, ratePeriod, TimeUnit.SECONDS,
                        null);
            }
        } else {
            removeRolloverLimiter();
        }
    }

    public void setConfig(ManagedLedgerFactoryConfig config) {
        // if config is changed, update rollover rate
        if (config.getRolloverThrottlingRate() != this.config.getRolloverThrottlingRate()
                && config.getRolloverRatePeriodInSecond() != this.config.getRolloverRatePeriodInSecond()) {
            this.config = config;
            onConfigUpdate(this.config);
            this.ledgerRolloverSchedule.updateRate(new RolloverRate(config.getRolloverThrottlingRate(),
                    config.getRolloverRatePeriodInSecond()));
        }
    }

    public void onConfigUpdate(ManagedLedgerFactoryConfig config) {
        RolloverRate rolloverRate = new RolloverRate(config.getRolloverThrottlingRate(),
                config.getRolloverRatePeriodInSecond());
        onRolloverRateUpdate(rolloverRate);
    }

    public void onRolloverRateUpdate(RolloverRate rolloverRate) {
        this.rolloverRate = rolloverRate;
        stopResetTask();
        if (!isRolloverRateEnabled(rolloverRate)) {
            removeRolloverLimiter();
        } else {
            updateRolloverRate(rolloverRate);
            this.resetTask = createTask();
            log.info("configured rollover rate at ledger {}", rolloverRate);
        }
    }

    public long getRolloverRateLimit() {
        return ledgerRolloverRateLimiter
                != null ? ledgerRolloverRateLimiter.getRate() : -1;
    }

    public void close() {
        closeRateLimiter();
        stopResetTask();
        singleton = null;
    }

    private synchronized void closeRateLimiter() {
        log.info("rollover rate limiter closed");
        if (this.ledgerRolloverRateLimiter != null) {
            this.ledgerRolloverRateLimiter.close();
            this.ledgerRolloverRateLimiter = null;
        }
        if (this.ledgersHasPerm != null) {
            this.ledgersHasPerm.clear();
        }
    }


    private static boolean isRolloverRateEnabled(RolloverRate rolloverRate) {
        return rolloverRate != null && (rolloverRate.rolloverThrottlingRate > 0);
    }

    private void stopResetTask() {
        if (resetTask != null) {
            this.resetTask.cancel(false);
        }
    }

    private ScheduledFuture<?> createTask() {
        return executorService.scheduleAtFixedRate(this::closeRateLimiter,
                this.rolloverRate.rolloverThrottlingRate,
                this.rolloverRate.ratePeriodInSecond,
                TimeUnit.SECONDS);
    }

    public RolloverRate getRolloverRate() {
        return rolloverRate;
    }

    private static final Logger log = LoggerFactory.getLogger(LedgerRolloverRateLimiter.class);
}

