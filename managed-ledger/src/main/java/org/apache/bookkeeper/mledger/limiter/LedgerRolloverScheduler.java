package org.apache.bookkeeper.mledger.limiter;

import io.netty.util.concurrent.DefaultThreadFactory;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.apache.bookkeeper.mledger.ManagedLedger;
import org.apache.pulsar.common.policies.data.RolloverRate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author SJQ
 */
public class LedgerRolloverScheduler {

    private static final int IDLING_CIRCLES = 5;
    private final ConcurrentLinkedQueue<ManagedLedger> ledgerReadyRollover;
    private final Set<ManagedLedger> ledgerSet;
    private final ScheduledExecutorService executorService;
    private ScheduledFuture<?> task;
    private long rateMs;
    private int count;

    public LedgerRolloverScheduler(RolloverRate rolloverRate) {
        this.ledgerReadyRollover = new ConcurrentLinkedQueue<>();
        this.executorService = new ScheduledThreadPoolExecutor(1,
                new DefaultThreadFactory("rollover-limiter-scheduler"));
        this.rateMs = getRate(rolloverRate);
        this.ledgerSet = Collections.synchronizedSet(new HashSet<>());
    }

    public long getRate(RolloverRate rolloverRate) {
        if (rolloverRate.rolloverThrottlingRate != -1) {
            // Multiply by 0.95 to ensure that the rollover can be completed within the period
            return (long) (Math.floor(1000 * 0.95 * (double) rolloverRate.ratePeriodInSecond
                   / rolloverRate.rolloverThrottlingRate));
        } else {
            return -1;
        }
    }

    public void add(ManagedLedger ledger) {
        if (this.task == null) {
            synchronized (LedgerRolloverScheduler.class) {
                if (this.task == null) {
                    log.info("Start Ledger rollover scheduler task");
                    this.task = createTask();
                }
            }
        }

        log.info("Add ledger [{}] to the queue", ledger.getName());
        ledgerReadyRollover.offer(ledger);
        ledgerSet.add(ledger);
    }

    public boolean contains(ManagedLedger ledger) {
        return ledgerSet.contains(ledger);
    }

    public ScheduledFuture<?> createTask() {
        return executorService.scheduleAtFixedRate(this::rolloverLedgersInQueue,
                rateMs, rateMs, TimeUnit.MILLISECONDS);
    }

    public void stopTask() {
        if (this.task != null) {
            log.info("Stop ledger rollover scheduler task");
            this.task.cancel(false);
        }
        if (LedgerRolloverRateLimiter.getLedgerRolloverRateLimiter() != null) {
            log.info("Try to stop ledger rollover rate limiter");
            LedgerRolloverRateLimiter limiter = LedgerRolloverRateLimiter.getLedgerRolloverRateLimiter();
            limiter.close();
        }
    }

    public void updateRate(RolloverRate rolloverRate) {
        if (rolloverRate.rolloverThrottlingRate <= 0) {
            // do-nothing, just follow the last set rate
            return;
        }
        if (this.task != null && !this.task.isDone()) {
            this.task.cancel(false);
        }
        this.rateMs = getRate(rolloverRate);
        this.task = this.createTask();
    }

    /**
     * rollover a ledger polled from the queue.
     */
    private void rolloverLedgersInQueue() {
        if (!ledgerReadyRollover.isEmpty()) {
            ManagedLedger ledger = ledgerReadyRollover.poll();
            log.info("Rollover ledger in queue: [{}]", ledger.getName());
            ledgerSet.remove(ledger);
            ledger.rollCurrentLedger();
        } else {
            log.info("The queue is empty: [{}]", this.count);
            if (count >= IDLING_CIRCLES) {
                count = 0;
                this.stopTask();
            }
            count++;
        }
    }

    private final Logger log = LoggerFactory.getLogger(LedgerRolloverScheduler.class);
}
