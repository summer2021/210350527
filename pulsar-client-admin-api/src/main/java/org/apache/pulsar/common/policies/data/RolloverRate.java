package org.apache.pulsar.common.policies.data;

import java.util.Objects;
import lombok.ToString;

/**
 * Information about rollover rate.
 */
@ToString
public class RolloverRate {

    public int rolloverThrottlingRate = -1;
    public int ratePeriodInSecond = 30;

    public RolloverRate() {
    }

    public RolloverRate(int rolloverThrottlingRate, int ratePeriodInSecond) {
        this.rolloverThrottlingRate = rolloverThrottlingRate;
        this.ratePeriodInSecond = ratePeriodInSecond;
    }

    @Override
    public int hashCode() {
        return Objects.hash(rolloverThrottlingRate, ratePeriodInSecond);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof RolloverRate) {
            RolloverRate rate = (RolloverRate) obj;
            return Objects.equals(rolloverThrottlingRate, rate.rolloverThrottlingRate)
                    && Objects.equals(ratePeriodInSecond, rate.ratePeriodInSecond);
        }
        return false;
    }

}
